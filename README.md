# DASH to IPTV #

DASH to IPTV is a software relay for the retransmission of live MPEG-DASH
content as an RTP stream to a multicast address in an attempt to allow ISPs to
make better use of their network than live MPEG-DASH content does.

A video demonstation of this project is available [here](https://www.youtube.com/watch?v=dbst1bmRCMM).

## Installation ##

### Dependencies ###

This software depends on:

- FFmpeg libav (v3.0.0 +)
- cmake

### Building dash2iptv ###

To build the relay, once all dependencies have been installed run `make` (or
`make -f Makefile.osx` for macOS) in the base directory. This will clone and
build a version of libdash (required for downloading the DASH stream) and then
build the main relay software. The  output of this is found in the `bin` folder.

## Running the relay ##

To run the relay, simply navigate to the `bin` folder and run the `dash2iptv`
executable. A sample MPD URL is hardcoded into the relay, but if you want to
retransmit a different DASH stream, provide the URL to the manifest file as a
command line argument to the program (e.g. `./dash2iptv
http://example.com/dash.mpd`).

When the relay starts, you will be prompted to choose the period, adaptation set
and representation from the DASH stream to be rebroadcast.

## Directory ##

The source tree is composed as follows:

- **eval/** - contains the script required for latency evaluations on the system
    - *latencyeval.py* - python script for extracting latencies from packet capture run during relay operation
    - *requirements.txt* - pip requirements file (install with `pip install -r requirements.txt`)
- **include/** - contains the header files for the libdash library
    - **libdash/** - details of this can be found at <https://github.com/bitmovin/libdash>
- **lib/** - where the libdash library will be cloned and built
    - *libdash.so*/*libdash.dylib* - (present once `make` has been run) the built libdash library
- **spikes/** - contains the source for the prototype programs built to test libdash and libav
    - **libav-trial/**
        - *Makefile*/*Makefile.osx* - makefile for building the libav-trial program
        - *libav-trial.cpp* - source code for the libav-trial program
    - **libdash-trial/**
        - *Makefile*/*Makefile.osx* - makefile for building the libdash-trial program
        - *libdash-trial.cpp* - source code for the libdash-trial program
- **src/** - the main source directory
    - *Makefile*/*Makefile.osx* - makefile for building the main relay
    - *\*.cpp* - source files for main relay program
    - *\*.h* - header files for main relay program

## Contact ##

David Welsh - 2083417w@student.gla.ac.uk *or* mail@davidwelsh.co.uk
