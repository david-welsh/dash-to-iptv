all: libdash-trial libav-trial dash2iptv

lib/libdash.so:
	git clone https://github.com/david-welsh/libdash.git lib/libdash-source
	mkdir -p lib/libdash-source/libdash/build;
	cd lib/libdash-source/libdash/build; cmake ..; make
	cp lib/libdash-source/libdash/build/bin/libdash.so lib

libdash-trial: lib/libdash.so spikes/libdash-trial/*
	mkdir -p bin
	cd spikes/libdash-trial; make
	mv spikes/libdash-trial/libdash-trial bin/libdash-trial

libav-trial: spikes/libav-trial/*
	mkdir -p bin
	cd spikes/libav-trial; make
	mv spikes/libav-trial/libav-trial bin/libav-trial

dash2iptv: src/* lib/libdash.so
	mkdir -p bin
	cd src; make
	mv src/dash2iptv bin/dash2iptv

clean:
	cd spikes/libdash-trial; make clean
	cd spikes/libav-trial; make clean
	cd src; make clean
	rm -rf lib/libdash-source
	rm -f lib/libdash.so
	rm -rf bin
