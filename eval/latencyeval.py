# ------------------------------------------------------------------------------
#                       dash2iptv Latency Evaluation
# ------------------------------------------------------------------------------
#     Reads output of tcpdump while dash2iptv is running to calculate latency
# ------------------------------------------------------------------------------

import dpkt
import hashlib, struct, sys

# Find TCP packet corresponding to index in reassembled TCP packets
def find_packet(packets, index):
    cursor = 0
    while index > 0:
        index = index - len(packets[cursor][0].data)
        cursor = cursor + 1
    return packets[cursor - 1]

if len(sys.argv) != 2:
    print("Incorrect arguments")
    sys.exit()

f = open(sys.argv[1], "rb")

cap = dpkt.pcap.Reader(f)

tcp_dict = {}
rtp_list = []

for ts, buf in cap:
    # Extract the ip frames from the SLL (May be specific to certain captures)
    sll = dpkt.sll.SLL(buf)
    ip = sll.data

    if type(ip) is str:
        continue
    pkt_contents = ip.data

    # Match RTP packets sent to port 9000 (as is default in relay)
    if type(pkt_contents) is dpkt.udp.UDP and pkt_contents.dport == 9000:
        rtp = dpkt.rtp.RTP(pkt_contents.data)
        # Throw away frames that only contain SEI (too short to match uniquely)
        if not(len(rtp.data) < 8):
            rtp_list.append((rtp,ts))
    # Match TCP packets of DASH downloads
    elif type(pkt_contents) is dpkt.tcp.TCP and len(pkt_contents.data) > 0:
        if pkt_contents.sport == 80:
            # Append to dictionary indexed on destination port for reassembly
            if pkt_contents.dport not in tcp_dict:
                tcp_dict[pkt_contents.dport] = {"packets": []}
            tcp_dict[pkt_contents.dport]["packets"].append((pkt_contents,ts))

for tcp_list in tcp_dict:
    cur = tcp_dict[tcp_list]
    # Sort and reassemble the packets then parse as HTTP
    cur["packets"].sort(key=lambda x: x[0].seq)
    cur["reasm"] = ''.join(z[0].data for z in cur["packets"])
    cur["http"] = dpkt.http.Response(cur["reasm"])
    for rtp_pkt in rtp_list:
        # Match Fragmentation Units
        if struct.unpack(">B",rtp_pkt[0].data[0])[0] & 0b11100 == 0b11100:
            # Ignore packets marked as filler
            if struct.unpack(">B", rtp_pkt[0].data[1])[0] & 0b00011111 == 0b01100:
                continue
            # Attempt to find the payload of the RTP packet in reassembled TCP
            ind = cur["reasm"].find(rtp_pkt[0].data[2:])
        # Ignore filler packets
        elif struct.unpack(">B", rtp_pkt[0].data[0])[0] & 0b01100 == 0b01100:
            continue
        else:
            # Attempt to find payload of RTP packet in reassembled TCP
            ind = cur["reasm"].find(rtp_pkt[0].data[1:])
        if ind != -1:
            # Find the TCP packet this came from
            pkt = find_packet(cur["packets"], ind)
            # Calculate time difference between packets
            diff = rtp_pkt[1] - pkt[1]
            print(diff)
