/*
 * =====================================================================================
 *
 *       Filename:  libdash-trial.cpp
 *
 *    Description:  Trial of libdash library to determine effectiveness for
 *					DASH to IPTV project
 *
 *        Version:  1.0
 *        Created:  12/10/16 11:32:52
 *       Compiler:  gcc
 *
 *         Author:  David Welsh
 *
 * =====================================================================================
 */

#include <libdash/libdash.h>
#include <libdash/IDASHManager.h>
#include <libdash/IMPD.h>
#include <libdash/ISegmentURL.h>
#include <libdash/IChunk.h>
#include <iostream>
#include <fstream>
#include <math.h>

using namespace dash;
using namespace dash::mpd;
using namespace dash::network;

//From qtsampleplayer libdash-framework
double GetDurationInSec    (const std::string& duration)
{
    /* no check for duration with yyyy,dd,mm */
    if (duration == "" || duration.substr(0, 2) != "PT")
        return 0;

    size_t      startPos    = 2;
    size_t      endPos      = std::string::npos;
    uint32_t    hours       = 0;
    uint32_t    mins        = 0;
    double      secs        = 0;

    char designators[] = { 'H', 'M', 'S' };

    endPos = duration.find(designators[0], startPos);
    if (endPos != std::string::npos)
    {
        hours = strtol(duration.substr(startPos, endPos - startPos).c_str(), NULL, 10);
        startPos = endPos + 1;
    }

    endPos = duration.find(designators[1], startPos);
    if (endPos != std::string::npos)
    {
        mins = strtol(duration.substr(startPos, endPos - startPos).c_str(), NULL, 10);
        startPos = endPos + 1;
    }

    endPos = duration.find(designators[2], startPos);
    if (endPos != std::string::npos)
        secs = strtod(duration.substr(startPos, endPos - startPos).c_str(), NULL);

    return hours*3600 + mins*60 + secs;
}


int main() {
  //char *path = "http://vod-dash-uk-live.akamaized.net/usp/auth/vod/piff_abr_full_hd/3d5e58-b084xjyw/vf_b084xjyw_b301d50e-54fa-4e44-861a-77c0de792535.ism/pc_hd_abr_v2_dash_master.mpd?__gda__=1481222410_06d3824680e76d44552e9616a2bb4592";
	// MPD file to read from
	char *path = "http://www-itec.uni-klu.ac.at/ftp/datasets/mmsys12/BigBuckBunny/MPDs/BigBuckBunnyNonSeg_2s_isoffmain_DIS_23009_1_v_2_1c2_2011_08_30.mpd";
	//char *path = "http://rdmedia.bbc.co.uk/dash/ondemand/bbb/2/client_manifest-common_init.mpd";
	//char *path = "http://yt-dash-mse-test.commondatastorage.googleapis.com/media/feelings_vp9-20130806-manifest.mpd"
  //char *path = "http://vod-dash-uk-live.bbcfmt.hs.llnwd.net/usp/auth/vod/piff_abr_full_hd/291d0e-b084jfy4/vf_b084jfy4_0becf6f0-4637-4836-b202-ef2dbab03504.ism/pc_hd_abr_v2_dash_master.mpd?s=1480621528&e=1480664728&h=3dabe69fbcd1e8333bc10bb9af7df750";
	std::string file_name_template = "period%d";

	// Create DASH manager and open the specified mpd
	IDASHManager *manager = CreateDashManager();
	IMPD *mpd = manager->Open(path);


	// Get list of periods from mpd
	std::vector<dash::mpd::IPeriod *>periods = mpd->GetPeriods();

	// Create file stream to write to
	std::ofstream fout;

	// Iterate through list of periods, getting video for each
	for (unsigned int i = 0; i < periods.size(); i++) {

		IAdaptationSet *adap = periods.at(i)->GetAdaptationSets().at(0);
		IRepresentation *rep = adap->GetRepresentation().at(18);

		// Get the base URL(s) from the mpd and use the first
		std::vector<dash::mpd::IBaseUrl *> baseurls;
		if (mpd->GetBaseUrls().size() > 0) {
			baseurls.push_back(mpd->GetBaseUrls().at(0));
		}
		if (periods.at(i)->GetBaseURLs().size() > 0) {
			baseurls.push_back(periods.at(i)->GetBaseURLs().at(0));
		}
		if (adap->GetBaseURLs().size() > 0) {
			baseurls.push_back(adap->GetBaseURLs().at(0));
		}

		if (baseurls.size() > 0) {
			if (baseurls.at(0)->GetUrl().substr(0,7) != "http://" && baseurls.at(0)->GetUrl().substr(0,8) != "https://") {
            	baseurls.insert(baseurls.begin(), mpd->GetMPDPathBaseUrl());
        	}
		} else {
			baseurls.push_back(mpd->GetMPDPathBaseUrl());
		}

		int type = 0;

		if (rep->GetSegmentList()) type = 1;
		else if (rep->GetSegmentTemplate() || adap->GetSegmentTemplate()) type = 2;
		else if (rep->GetSegmentBase() || rep->GetBaseURLs().size() > 0) type = 3;

		ISegmentList *seg_list = NULL;
		ISegmentTemplate *seg_template = NULL;
		std::vector<ISegment *> segments;
		std::vector<ISegmentURL *> segment_urls;

		switch (type) {
			case 1:
			{
				seg_list = rep->GetSegmentList();
				if (seg_list) {
					segment_urls = rep->GetSegmentList()->GetSegmentURLs();
				}
				for (size_t l = 0; l < segment_urls.size(); l++) {
					segments.push_back(segment_urls.at(l)->ToMediaSegment(baseurls));
				}
				break;
			}
			case 2:
			{
				if (rep->GetSegmentTemplate()) {
					seg_template = rep->GetSegmentTemplate();
				} else {
					seg_template = adap->GetSegmentTemplate();
				}
				double durationInSec = GetDurationInSec(mpd->GetMediaPresentationDuration());
				int numberOfSegments = (uint32_t) ceil(durationInSec / (seg_template->GetDuration() / seg_template->GetTimescale()));
				ISegment *initSegment = seg_template->ToInitializationSegment(baseurls, rep->GetId(), rep->GetBandwidth());
				if (initSegment) {
					segments.push_back(initSegment);
				}
				for (int k = 0; k < numberOfSegments; k++) {
					ISegment *new_segment = seg_template->GetMediaSegmentFromNumber(baseurls, rep->GetId(), rep->GetBandwidth(), k);
					segments.push_back(new_segment);
				}
				break;
			}
            case 3:
            {
                ISegment *init_segment = rep->GetBaseURLs().at(0)->ToMediaSegment(baseurls);
                //ISegment *rep_segment = rep->GetSegmentBase()->GetRepresentationIndex()->ToSegment(baseurls);
                segments.push_back(init_segment);
                //segments.push_back(rep_segment);
                break;
            }
			default:
				break;
		}


		std::cerr << "Getting segments for period " << i << "..." << std::endl;

		ISegmentBase *rep_seg_base = rep->GetSegmentBase();

		ISegment *base_seg;
		if (rep_seg_base) {
			base_seg = rep->GetSegmentBase()->GetInitialization()->ToSegment(baseurls);
		} else {
			base_seg = NULL;
		}

		// Generate file name and open file
		char *file_name = new char[file_name_template.length() + 32];
		sprintf(file_name, file_name_template.c_str(), i);
		fout.open(file_name, std::ios::binary | std::ios::out);

		dash::mpd::ISegment *segment;
		uint8_t *data = new uint8_t[32768];

		// Get the base segment if it exists
		if (base_seg) {
			base_seg->StartDownload();

			int p = base_seg->Read(data, 32768);
			while (p) {
				fout.write((char *) data, p);
                //std::cout.write((char *) data, p);
				p = base_seg->Read(data, 32768);
			}
		}

		for (unsigned int j = 0; j < segments.size(); j++) {
			std::cerr << "\r" << "Downloading period " << i << " [ " << j + 1 << "/" << segments.size() << "]" << std::flush;
			segment = segments.at(j);
			segment->StartDownload();
			int p = segment->Read(data, 32768);
			while (p) {
				fout.write((char *) data, p);
                //std::cout.write((char *) data, p);
				p = segment->Read(data, 32768);
			}
		}


		fout.close();
	}

	return 0;
}
