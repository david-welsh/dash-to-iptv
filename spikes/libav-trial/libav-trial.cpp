/*
 * =====================================================================================
 *
 *       Filename:  libav-trial.cpp
 *
 *    Description:  Trial of libav to determine effectiveness for DASH to IPTV project
 *
 *        Version:  1.0
 *        Created:  19/10/16 16:20:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  David Welsh	
 *
 * =====================================================================================
 */

#include <iostream>
#include <unistd.h>
#include <fstream>

extern "C" {
	#include <libavcodec/avcodec.h>
	#include <libavformat/avformat.h>
    #include <libavutil/avutil.h>
}


int main(int argc, char *argv[]) {

    const char *outputUrl = "rtp://127.0.0.1:9077";

	if (argc < 2) {
		std::cerr << "No video file provided..." << std::endl;
		return -1;
	}

	// Register all codecs available
	av_register_all();
    avformat_network_init();

	AVFormatContext *formatCtx = NULL;

	// Open file specified as argument, if it can't be opened exit
	if (avformat_open_input(&formatCtx, argv[1], NULL, NULL) != 0) {
		std::cerr << "Couldn't open file..." << std::endl;
		return -1;
	}

	// Read the stream information from the file
	//  formatCtx->streams populated
	if (avformat_find_stream_info(formatCtx, NULL) < 0 ) {
		std::cerr << "Couldn't read stream info..." << std::endl;
		return -1;
	}

	av_dump_format(formatCtx, 0, argv[1], 0);


	int videoStream = -1;

	for (unsigned int i = 0; i < formatCtx->nb_streams; i++) {
		if (formatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
			videoStream = i;
			break;
		}
	}
	if (videoStream == -1) {
		std::cerr << "Couldn't find video stream..." << std::endl;
		return -1;
	}

	AVFormatContext *outputCtx = NULL;

	avformat_alloc_output_context2(&outputCtx, NULL, "rtp", outputUrl);

	enum AVCodecID codecId = formatCtx->streams[videoStream]->codecpar->codec_id;

	AVStream *outStream = avformat_new_stream(outputCtx, avcodec_find_encoder(codecId));
    avcodec_parameters_copy(outStream->codecpar, formatCtx->streams[videoStream]->codecpar);

	av_dump_format(outputCtx, 0, outputUrl, 1);

	int ret;

	avio_open(&outputCtx->pb, outputUrl, AVIO_FLAG_WRITE);

    char sdp[1024];
    av_sdp_create(&outputCtx, 1, sdp, 1024);
    std::ofstream fout;
    fout.open("./stream.sdp");
    fout.write(sdp, strlen(sdp));
    fout.close();

    outputCtx->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL;
		


	ret = avformat_write_header(outputCtx, NULL);

	if (ret < 0) return -1;

	AVPacket pkt;

	while (1) {
        AVStream *inStream, *outStream;
        
		int frame = av_read_frame(formatCtx, &pkt);
        if (frame < 0) break;
        
        inStream = formatCtx->streams[videoStream];
        outStream = outputCtx->streams[0];

        std::cout << pkt.pts;

        av_packet_rescale_ts(&pkt, inStream->time_base, outStream->time_base);
        std::cout << " : " << pkt.pts << std::endl;
/* 
        pkt.pts = av_rescale_q_rnd(pkt.pts, inStream->time_base, outStream->time_base, 
            AV_ROUND_NEAR_INF);
        pkt.dts = av_rescale_q_rnd(pkt.dts, inStream->time_base, outStream->time_base,
            AV_ROUND_NEAR_INF);
        pkt.duration = av_rescale_q(pkt.duration, inStream->time_base, outStream->time_base);*/
        pkt.pos = -1;


        AVRational f = av_mul_q(outStream->time_base, (AVRational){1000000,1});
        int64_t dur = av_rescale_q(pkt.duration, f, (AVRational){1,1});
        ret = av_interleaved_write_frame(outputCtx, &pkt);


        usleep(dur);
	} 

	return 0;

}
