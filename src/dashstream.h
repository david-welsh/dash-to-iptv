// -----------------------------------------------------------------------------
//
//  dashstream.h
//      Definition of DashStream object and related functions
//      Manages data pertaining to incoming DASH stream and downloading of
//        said stream
//
//   Author: David Welsh - mail@davidwelsh.co.uk
//
// -----------------------------------------------------------------------------

#ifndef _DASHSTREAM_H_
#define _DASHSTREAM_H_

#include <libdash/libdash.h>
#include <ctime>
#include <queue>
#include <math.h>
#include <thread>
#include <mutex>
#include <condition_variable>

#include "input.h"
#include "DownloadObserver.h"

#define BUFFER_THRESHOLD 5

enum DashStreamType { SINGLE_SEGMENT, SEGMENT_LIST, SEGMENT_TEMPLATE };

struct DashStream {
  dash::mpd::IMPD *mpd;
  dash::mpd::IPeriod *period;
  dash::mpd::IAdaptationSet *adap;
  dash::mpd::IRepresentation *rep;
  dash::network::IDownloadObserver *downObs;
  DashStreamType streamType;
  std::vector<dash::mpd::IBaseUrl *> baseUrls;
  std::queue<dash::mpd::ISegment *> toBuffer;
  std::queue<dash::mpd::ISegment *> bufferedSegments;
  size_t nextSegmentNumber;

  std::thread *downloadThread;

  std::mutex m;
  std::condition_variable cv;

  std::mutex bufferMutex;
  std::condition_variable bufferCv;

};

DashStream *dashStreamCreate(char *mpdURL);

void dashStreamDestroy(DashStream *stream);

dash::mpd::ISegment *getFirstSegment(DashStream *stream);

dash::mpd::ISegment *getSegment(DashStream *stream);

void bufferStream(DashStream *stream);

#endif /* _DASHSTREAM_H_ */
