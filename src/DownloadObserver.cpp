// -----------------------------------------------------------------------------
//
//  DownloadObserver.cpp
//      Implementation of DownloadObserver class
//      Checks for completion of download of segments
//
//   Author: David Welsh - mail@davidwelsh.co.uk
//
// -----------------------------------------------------------------------------

#include "DownloadObserver.h"

DownloadObserver::DownloadObserver() {

}

void DownloadObserver::OnDownloadStateChanged(dash::network::DownloadState state) {
  std::unique_lock<std::mutex> lk(m);

  // Update state held by observer and notify of change
  this->state = state;

  downloadCv.notify_one();
}

void DownloadObserver::OnDownloadRateChanged(uint64_t bytesDownloaded) {
}

void DownloadObserver::WaitForDownload(std::condition_variable *cv) {
  std::unique_lock<std::mutex> lk(m);

  // Wait until download is finished
  while (this->state != dash::network::COMPLETED)
    downloadCv.wait(lk);

  cv->notify_one();
}
