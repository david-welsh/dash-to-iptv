// -----------------------------------------------------------------------------
//
//  dashstream.cpp
//      Implementation of DashStream object and related functions
//      Manages data pertaining to incoming DASH stream and downloading of
//        said stream
//
//   Author: David Welsh - mail@davidwelsh.co.uk
//
// -----------------------------------------------------------------------------

#include "dashstream.h"

using namespace dash;
using namespace dash::mpd;

// -----------------------------------------------------------------------------
//                            DASH functions
// -----------------------------------------------------------------------------

DashStreamType getStreamType(IAdaptationSet *adap, IRepresentation *rep) {
  DashStreamType streamType;
  if (rep->GetSegmentList()) {
    streamType = SEGMENT_LIST;
    std::cout << "Period structured as segment list..." << std::endl;
  } else if (rep->GetSegmentTemplate() || adap->GetSegmentTemplate()) {
    streamType = SEGMENT_TEMPLATE;
    std::cout << "Period structured as segment template..." << std::endl;
  } else {
    streamType = SINGLE_SEGMENT;
    std::cout << "Period structured as single segment..." << std::endl;
  }
  return streamType;
}

std::vector<IBaseUrl *> getBaseUrls(IMPD *mpd, IPeriod *period,
  IAdaptationSet *adap) {
    std::vector<IBaseUrl *> baseUrls;

    // Generate list of base URLs
    if (mpd->GetBaseUrls().size() > 0)
      baseUrls.push_back(mpd->GetBaseUrls().at(0));
    if (period->GetBaseURLs().size() > 0)
      baseUrls.push_back(period->GetBaseURLs().at(0));
    if (adap->GetBaseURLs().size() > 0)
      baseUrls.push_back(adap->GetBaseURLs().at(0));

    // If base URLs are relative, add the MPD path base URL
    // If no base URLs were found, use only the MPD path base URL
    if (baseUrls.size() > 0) {
      if (baseUrls.at(0)->GetUrl().substr(0,7) != "http://"
        && baseUrls.at(0)->GetUrl().substr(0,8) != "https://") {
          baseUrls.insert(baseUrls.begin(), mpd->GetMPDPathBaseUrl());
        }
    } else {
      baseUrls.push_back(mpd->GetMPDPathBaseUrl());
    }
    return baseUrls;
  }

ISegment *getInitSegment(DashStreamType streamType, IAdaptationSet *adap,
  IRepresentation *rep, std::vector<IBaseUrl *> baseUrls) {
    ISegment *initSegment;
    switch(streamType) {
      case SEGMENT_LIST:
      {
        ISegmentBase *repSegBase = rep->GetSegmentBase();
        if (repSegBase) {
          initSegment = repSegBase->GetInitialization()->ToSegment(baseUrls);
        } else {
          initSegment = NULL;
        }
        break;
      }
      case SEGMENT_TEMPLATE:
      {
        ISegmentTemplate *segTemplate;
        if (rep->GetSegmentTemplate()) {
          segTemplate = rep->GetSegmentTemplate();
        } else {
          segTemplate = adap->GetSegmentTemplate();
        }

        initSegment = segTemplate->ToInitializationSegment(baseUrls,
          rep->GetId(), rep->GetBandwidth());
        break;
      }
      case SINGLE_SEGMENT:
      {
        initSegment = rep->GetBaseURLs().at(0)->ToMediaSegment(baseUrls);
        break;
      }
      default:
        break;
    }
    return initSegment;
  }

ISegment *getSegment(DashStream *stream) {
    ISegment *segment = NULL;
    switch(stream->streamType) {
      case SINGLE_SEGMENT:
      {
        segment = NULL;
        break;
      }
      case SEGMENT_TEMPLATE:
      {
        ISegmentTemplate *segTemplate;
        if (stream->rep->GetSegmentTemplate())
          segTemplate = stream->rep->GetSegmentTemplate();
        if (stream->adap->GetSegmentTemplate())
          segTemplate = stream->adap->GetSegmentTemplate();
        if (stream->period->GetSegmentTemplate())
          segTemplate = stream->period->GetSegmentTemplate();
        segment = segTemplate->GetMediaSegmentFromNumber(stream->baseUrls,
          stream->rep->GetId(), stream->rep->GetBandwidth(),
          stream->nextSegmentNumber);
        stream->nextSegmentNumber += 1;
        break;
      }
      case SEGMENT_LIST:
      {
        ISegmentList *segList = stream->rep->GetSegmentList();
        std::vector<ISegmentURL *> segURLs = segList->GetSegmentURLs();
        if (stream->nextSegmentNumber > segURLs.size()) {
          segment = NULL;
        } else {
          segment = segURLs.at(stream->nextSegmentNumber - 1)->ToMediaSegment(stream->baseUrls);
          stream->nextSegmentNumber++;
        }
        break;
      }
      default:
        break;
    }
    return segment;
  }

size_t getFirstSegmentNumber(DashStream *stream) {
  if (stream->mpd->GetType() == "dynamic") {
    // Calculate first segment from current time
    // TODO: clean up
    ISegmentTemplate *segTemplate;
    if (stream->rep->GetSegmentTemplate())
      segTemplate = stream->rep->GetSegmentTemplate();
    if (stream->adap->GetSegmentTemplate())
      segTemplate = stream->adap->GetSegmentTemplate();
    if (stream->period->GetSegmentTemplate())
      segTemplate = stream->period->GetSegmentTemplate();
    std::time_t currentTime = std::time(nullptr);
    uint32_t timescale = segTemplate->GetTimescale();
    uint32_t duration = segTemplate->GetDuration();
    double x = duration / (double)timescale;
    return (currentTime / x) - 20;
  } else {
    return 1;
  }
}

void bufferStream(DashStream *stream) {
  ISegment *nextSegment = NULL;
  DownloadObserver *observer = new DownloadObserver();
  if (!stream->toBuffer.empty()) {
    nextSegment = stream->toBuffer.front();
    stream->toBuffer.pop();
  }

  while (nextSegment) {
    std::unique_lock<std::mutex> lk(stream->m);
    if (stream->bufferedSegments.size() > BUFFER_THRESHOLD) {
      stream->bufferCv.wait(lk);
    }
    nextSegment->AttachDownloadObserver(observer);
    nextSegment->StartDownload();
    observer->WaitForDownload(&stream->cv);
    nextSegment->DetachDownloadObserver(observer);
    stream->bufferedSegments.push(nextSegment);
    stream->toBuffer.push(getSegment(stream));
    nextSegment = stream->toBuffer.front();
    stream->toBuffer.pop();
  }

  delete observer;

}

// -----------------------------------------------------------------------------

DashStream *dashStreamCreate(char *mpdURL) {
  IDASHManager *dashManager;
  IMPD *dashMPD;
  DashStream *dashStream;
  ISegment *initSegment;

  // Load MPD
  dashManager = CreateDashManager();
  try {
    dashMPD = dashManager->Open(mpdURL);
  } catch (...) {
    std::cerr << "Couldn't open MPD..." << std::endl;
    std::cerr << "Exiting..." << std::endl;
    exit(-1);
  }

  dashStream = new DashStream();
  if (dashStream) {
    dashStream->mpd = dashMPD;
    dashStream->period = inputPeriod(dashStream->mpd);
    dashStream->adap = inputAdap(dashStream->period);
    dashStream->rep = inputRep(dashStream->adap);

    dashStream->streamType = getStreamType(dashStream->adap, dashStream->rep);
    dashStream->baseUrls = getBaseUrls(dashStream->mpd, dashStream->period,
      dashStream->adap);

    initSegment = getInitSegment(dashStream->streamType, dashStream->adap,
      dashStream->rep, dashStream->baseUrls);
    if (initSegment) {
      dashStream->toBuffer.push(initSegment);
    }

    dashStream->nextSegmentNumber = getFirstSegmentNumber(dashStream);
    if (dashStream->streamType != SINGLE_SEGMENT) {
      for (int i = 0; i < 2; i++) {
        dashStream->toBuffer.push(getSegment(dashStream));
      }
    }

  }
  return dashStream;
}

void dashStreamDestroy(DashStream *stream) {
  delete stream;
}
