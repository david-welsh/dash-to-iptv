// -----------------------------------------------------------------------------
//
//  DownloadObserver.h
//      Definition of DownloadObserver object and related functions
//      Checks for completion of download of segments
//
//   Author: David Welsh - mail@davidwelsh.co.uk
//
// -----------------------------------------------------------------------------

#ifndef _DOWNLOAD_OBS_H_
#define _DOWNLOAD_OBS_H_

#include <libdash/libdash.h>
#include <thread>
#include <mutex>
#include <condition_variable>

class DownloadObserver : public dash::network::IDownloadObserver {
public:
  DownloadObserver();
  virtual ~DownloadObserver() {};
  virtual void OnDownloadStateChanged (dash::network::DownloadState state);
  virtual void OnDownloadRateChanged (uint64_t bytesDownloaded);
  void WaitForDownload(std::condition_variable *cv);
private:
  dash::network::DownloadState state;
  std::condition_variable downloadCv;
  std::mutex m;
};

#endif /*_DOWNLOAD_OBS_H_*/
