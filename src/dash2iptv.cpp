// -----------------------------------------------------------------------------
//
//  dash2iptv.cpp
//      DASH to IPTV stream relay - Main program file
//
//   Author: David Welsh - mail@davidwelsh.co.uk
//
// -----------------------------------------------------------------------------

#include <libdash/libdash.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include <thread>

extern "C" {
	#include <libavcodec/avcodec.h>
	#include <libavformat/avformat.h>
  #include <libavutil/avutil.h>
}

#include "dashstream.h"

#define DEFAULT_MPD "http://rdmedia.bbc.co.uk/dash/ondemand/bbb/2/client_manifest-common_init.mpd"
#define DEFAULT_OUTPUT "rtp://127.0.0.1:9000"
#define BUFFER_SIZE 65535

using namespace dash;
using namespace dash::mpd;

// -----------------------------------------------------------------------------
//                        Custom AVIOContext Functions
// -----------------------------------------------------------------------------

int readBufferAVIO(void *ptr, uint8_t *buf, int buf_size) {
  DashStream *stream = static_cast<DashStream *>(ptr);
  int dataRead;
  while (stream->bufferedSegments.empty())
    continue;
  dataRead = stream->bufferedSegments.front()->Read(buf, buf_size);
  if (!dataRead) {
		delete stream->bufferedSegments.front();
    stream->bufferedSegments.pop();
    stream->bufferCv.notify_one();
    while (stream->bufferedSegments.empty())
      continue;
    dataRead = stream->bufferedSegments.front()->Read(buf, buf_size);
  }
  return dataRead;
}

// -----------------------------------------------------------------------------

int main(int argc, char *argv[]) {
  char *mpdURL;
  DashStream *dashStream;

  unsigned char *buffer = (unsigned char *)av_malloc(BUFFER_SIZE);

  AVIOContext *inputContext;
  AVFormatContext *formatCtx;
  AVFormatContext *outputCtx;
  AVStream *outStream;
  AVPacket *pkt;
  AVStream *inStream;
  enum AVCodecID codecId;
  int videoStream;

  char sdp[1024];
  std::ofstream fout;

  // Check arguments for MPD URL
  if (argc == 1) {
    std::cerr << "No MPD URL provided, using default..." << std::endl;
    mpdURL = (char *)DEFAULT_MPD;
  } else if (argc == 2) {
    mpdURL = argv[1];
  } else {
    std::cerr << "Unrecognised arguments, exiting..." << std::endl;
    exit(-1);
  }

  // Create DASH stream
  dashStream = dashStreamCreate(mpdURL);

  std::thread downloadThread(bufferStream, dashStream);

  // Initialize libav and demuxer
  av_register_all();
  avformat_network_init();

  // Create custom AVIO Context
  inputContext = avio_alloc_context(buffer, BUFFER_SIZE, 0,
    dashStream, readBufferAVIO, 0, 0);
	if (!inputContext) {
		std::cerr << "Couldn't alloc input context..." << std::endl;
		exit(-1);
	}
  inputContext->seekable = 0;

  // Create input context
  formatCtx = avformat_alloc_context();
	if (!formatCtx) {
		std::cerr << "Couldn't alloc input format context..." << std::endl;
		exit(-1);
	}
  // Set up to use custom AVIO Context
  formatCtx->pb = inputContext;
  formatCtx->flags = AVFMT_FLAG_CUSTOM_IO;

  // Try to open the DASH stream
  if (avformat_open_input(&formatCtx, NULL, NULL, NULL)) {
    std::cerr << "Couldn't open DASH stream..." << std::endl;
    exit(-1);
  }

  if (avformat_find_stream_info(formatCtx, NULL) < 0 ) {
    std::cerr << "Couldn't read stream info..." << std::endl;
    exit(-1);
  }

  // Print input information
  av_dump_format(formatCtx, 0, NULL, 0);

  // Find the video stream
  videoStream = -1;

  for (unsigned int i = 0; i < formatCtx->nb_streams; i++) {
    if (formatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
      videoStream = i;
      break;
    }
  }
  if (videoStream == -1) {
    std::cerr << "Couldn't find video stream..." << std::endl;
    exit(-1);
  }

  // Create the output context with stream and correct codec
  if (avformat_alloc_output_context2(&outputCtx, NULL, "rtp", DEFAULT_OUTPUT) < 0) {
		std::cerr << "Couldn't alloc output context..." << std::endl;
		exit(-1);
	}

  codecId = formatCtx->streams[videoStream]->codecpar->codec_id;

  outStream = avformat_new_stream(outputCtx,
    avcodec_find_encoder(codecId));
	if (!outStream) {
		std::cerr << "Couldn't create new stream..." << std::endl;
		exit(-1);
	}
  avcodec_parameters_copy(outStream->codecpar,
    formatCtx->streams[videoStream]->codecpar);

  // Print output information
  av_dump_format(outputCtx, 0, DEFAULT_OUTPUT, 1);

  // Open RTP stream
  if (avio_open(&outputCtx->pb, DEFAULT_OUTPUT, AVIO_FLAG_WRITE) < 0) {
		std::cerr << "Couldn't open output stream..." << std::endl;
		exit(-1);
	}

  // Write the SDP file
  av_sdp_create(&outputCtx, 1, sdp, 1024);
  fout.open("./stream.sdp");
  fout.write(sdp, strlen(sdp));
	fout << std::endl;
  fout.close();

  // Set up for VP9 streaming
  outputCtx->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL;

  // Initialise RTP stream
  if (avformat_write_header(outputCtx, NULL) < 0) {
    std::cerr << "Couldn't write headers..." << std::endl;
    exit (-1);
  }

  pkt = av_packet_alloc();
  AVRational f = av_mul_q(outStream->time_base, (AVRational){1000000,1});
  int64_t dur;

  // Output stream to RTP
  while (1) {
		clock_t t = clock();
    if (av_read_frame(formatCtx, pkt) < 0) {
      std::cerr << "No frame found, ending stream..." << std::endl;
      break;
    }

		// Get the approprate video streams
    inStream = formatCtx->streams[videoStream];
    outStream = outputCtx->streams[0];

		// Set timings for packet
    av_packet_rescale_ts(pkt, inStream->time_base, outStream->time_base);
    pkt->pos = -1;

		// Rescale duration to real time
    dur = av_rescale_q(pkt->duration, f, (AVRational){1,1});

    av_interleaved_write_frame(outputCtx, pkt);

		t = clock() - t;

    usleep(dur - t);
  }

	av_free(buffer);
  av_packet_free(&pkt);
  dashStreamDestroy(dashStream);

  return 0;
}
