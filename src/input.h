// -----------------------------------------------------------------------------
//
//  input.cpp
//      Defintion of user input functions
//
//   Author: David Welsh - mail@davidwelsh.co.uk
//
// -----------------------------------------------------------------------------

#ifndef _INPUT_H_
#define _INPUT_H_

#include <libdash/libdash.h>

int getIntInput(int max);

dash::mpd::IPeriod *inputPeriod(dash::mpd::IMPD *mpd);
dash::mpd::IAdaptationSet *inputAdap(dash::mpd::IPeriod *period);
dash::mpd::IRepresentation *inputRep(dash::mpd::IAdaptationSet *adap);

#endif /* _INPUT_H_ */
