// -----------------------------------------------------------------------------
//
//  input.cpp
//      Implementation of input.h header
//      Implements functions for user input
//
//   Author: David Welsh - mail@davidwelsh.co.uk
//
// -----------------------------------------------------------------------------

#include "input.h"

using namespace dash;
using namespace dash::mpd;

int getIntInput(int max) {
  std::stringstream ss;
  std::string choice;
  int returnChoice;

  // Loop until valid option entered
  while (true) {
    // Get input
    std::cout << "> ";
    std::getline(std::cin, choice);

    // Parse as int
    ss.str(choice);
    ss >> returnChoice;

    // Detect invalid inputs, and out of bounds inputs
    if (ss.fail()) {
      std::cerr << "Not a valid input..." << std::endl;
      ss.clear();
    } else if (returnChoice > max || returnChoice < 0){
      std::cerr << "Choice out of range..." << std::endl;
      ss.clear();
    } else {
      return returnChoice;
    }
  }
}

IPeriod *inputPeriod(IMPD *mpd) {
  IPeriod *chosenPeriod;
  int choice;
  int i;

  std::cout << "Select a period to rebroadcast..." << std::endl;
  // Print available periods
  i = 0;
  for (IPeriod *period : mpd->GetPeriods()) {
    std::cout << "(" << i++ << ")";
    std::cout << "ID: " << period->GetId() << ", duration: "
      << period->GetDuration() << std::endl;
  }

  // Get user choice
  choice = getIntInput(--i);
  chosenPeriod = mpd->GetPeriods().at(choice);

  return chosenPeriod;
}

IAdaptationSet *inputAdap(IPeriod *period) {
  IAdaptationSet *chosenAdap;
  int choice;
  int i;

  std::cout << "Select an adaptation set to rebrodacast..." << std::endl;
  // Print available adaptation sets
  i = 0;
  for (IAdaptationSet *adap : period->GetAdaptationSets()) {
    std::cout << "(" << i++ << ")";
    std::cout << "ID: " << adap->GetId() << ", Content: "
      << adap->GetContentType() << std::endl;
  }

  // Get user choice
  choice = getIntInput(--i);
  chosenAdap = period->GetAdaptationSets().at(choice);

  return chosenAdap;
}

IRepresentation *inputRep(IAdaptationSet *adap) {
  IRepresentation *chosenRep;
  int choice;
  int i;

  std::cout << "Select a representation to rebrodacast..." << std::endl;
  // Print available representations
  i = 0;
  for (IRepresentation *rep : adap->GetRepresentation()) {
    std::cout << "(" << i++ << ")";
    std::cout << "ID: " << rep->GetId() << ", Resolution: "
      << rep->GetWidth() << "x" << rep->GetHeight() << std::endl;
  }

  // Get user choice
  choice = getIntInput(--i);
  chosenRep = adap->GetRepresentation().at(choice);

  return chosenRep;
}
